apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: hive
  name: &name janitor
  labels: &labels
    project: janitor-service
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  replicas: 1
  selector:
    matchLabels:
      deployment: *name
  template:
    metadata:
      labels:
        deployment: *name
    spec:
      dnsPolicy: ClusterFirstWithHostNet
      volumes:
        - name: devvolume
          hostPath:
            path: ${HIVEDIR}
        - name: nodemodules
          persistentVolumeClaim:
            claimName: node-modules-pvc
      containers:
        - name: janitor
          image: swivel_dev_image
          imagePullPolicy: IfNotPresent
          command:
            - node
            - --inspect=9223
            - -r
            - ts-node/register
            - ./launch.ts
            - --hot
            - janitor
          volumeMounts:
            - mountPath: /usr/src/app
              name: devvolume
            - mountPath: /usr/src/app/node_modules
              name: nodemodules
          resources:
            requests:
              cpu: "150m"
              memory: "256Mi"
            limits:
              cpu: "300m"
              memory: "512Mi"
          env:
            - name: DEBUG
              value: "*"
            - name: DEV
              value: "${DEV}"
            - name: LIVENESS_PORT
              value: "8008"
            - name: TRANSPORTER
              value: nats://host.docker.internal:4222

          # When to restart the container
          # livenessProbe:
          #   httpGet:
          #     path: /
          #     port: 8008
          #   periodSeconds: 5
          #   initialDelaySeconds: 30
# We should not autoscale the janitor.  It is a low utilization
# service that should easily be able to handle any size cluster
